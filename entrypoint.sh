#!/bin/bash
echo "iniciando o entrypoint a partir do Dockerfile"
cd /app/src/main/webapp/WEB-INF/classes/pack/
javac *.java
cd /app/src/main/webapp/
jar cvf JavaWeb.war *
ls -ltra JavaWeb.war
mv JavaWeb.war /usr/local/tomcat/webapps/
ls -ltra /usr/local/tomcat/webapps/
chmod +x /usr/local/tomcat/bin/catalina.sh
/usr/local/tomcat/bin/catalina.sh run
